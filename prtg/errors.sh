#!/bin/bash
errors=$(grep "ERROR" /mnt/ramdisk/servers/$1/logs/latest.log | wc -l)

if [ $errors -gt 50 ]
then
        echo "1:$errors:$errors erreurs dans les logs"
fi

if [ $errors -lt 50 ]
then
        echo "0:$errors:$errors erreurs dans les logs"
fi