#!/bin/bash

declare -i tps
variable=$(docker exec $1 rcon-cli tps | grep -E -o '([0-2][0-9]?\.)' | head -1 | grep -E -o '([0-2][0-9]?)')

if [ $? -ne 1 ]
then
        tps=$variable
fi
#echo  "$(( tps + tps ))"

#echo "0:$tps:$tps"

if [ $tps -lt 19 ]
then
        echo "1:$tps:$tps"
fi

if [ $tps -gt 19 ]
then
        echo "0:$tps:$tps"
fi
