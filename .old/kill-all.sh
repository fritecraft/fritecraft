#!/bin/bash
docker stop mc_proxy && sleep 10
docker stop mc_hub && sleep 10
docker stop mc_bouquenia && sleep 10
docker stop mc_survie && sleep 10
docker stop mc_aventure && sleep 10
docker stop mc_ressources && sleep 10
docker stop mc_creatif && sleep 10
docker stop mc_vanilla && sleep 10
