#!/bin/bash

#Login MySQL
USERNAME=$(cat /root/username)
PASSWORD=$(cat /root/password)

#Couleurs rouge
RED='\033[0;31m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m' #Pas de couleurs


#Configuration des variables
backup_path="/mnt/data/_backups"
date=$(date +"%y-%m-%d")

# Afficher un texte, en couleur
echo -e "${PURPLE}Sauvegarde du serveur !${NC}"
# Notification Discord du début de sauvegarde
# python3 /root/discord.py ":information_source: Sauvegarde du serveur en cours... :hourglass:"

# Calcul espace libre
#DISQUE=$(df --output=avail / | grep [0-9])
#DISQUE=$(echo "${DISQUE::-6}")
#python3 /root/discord.py ":information_source: $DISQUE Go de libre"

#if [ $DISQUE -gt 20 ]
#then
#Compresser le serveur
echo -e "${BLUE} Compression des plugins en cours... ${NC}"
tar -czvf $backup_path/backup_bouquenia-"$date.tar.gz" /mnt/data/bouquenia/
tar -czvf $backup_path/backup_hub-"$date.tar.gz" /mnt/data/hub/
tar -czvf $backup_path/backup_proxy-"$date.tar.gz" /mnt/data/proxy/
tar -czvf $backup_path/backup_pitman-"$date.tar.gz" /mnt/data/pitman/


echo -e "${PURPLE} Serveur sauvegardé !${NC}"

#Notification Discord de sauvegarde avec taille
#ARCHIVE=$(du -h /home/minecraft/backups/*.gz -a | cut -d / -f1 | tail -n 1)
#python3 /root/discord.py ":white_check_mark: backup_server-$date.tar.gz      $ARCHIVE"

#Compresser les mondes
#echo -e "${BLUE} Compression des mondes en cours... ${NC}"
#tar -czf $backup_path/backup_worlds-"$date.tar.gz" /home/minecraft/fritecraft/ --exclude 'tiles/*' --exclude 'plugins/*'
#echo -e "${PURPLE} Mondes sauvegardés !${NC}}"

#Sauvergarder la base de données
#echo -e "${BLUE} Sauvegarde de la base de données en cours... ${NC}"
#mysqldump --all-databases > $backup_path/backup_base-$date.sql -u $USERNAME -p$PASSWORD
#echo -e "${PURPLE} Base de données sauvegardée !${NC}"

# Notification Discord de la BDD
#BDD=$(du -h /home/minecraft/backups/*.sql -a | cut -d / -f1 | tail -n 1)
#python3 /root/discord.py ":white_check_mark: backup_base-$date.sql            $BDD"


#else
#        python3 /root/discord.py ":warning: **ESPACE INSUFFISANT**"
#fi
