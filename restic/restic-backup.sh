#!/bin/bash

source "$(dirname -- "$0")/config.sh"

# Backup files
restic backup \
	--quiet \
	--option b2.connections=$B2_CONNECTIONS \
	$BACKUP_EXCLUDE \
	$BACKUP_PATHS &
wait $!
