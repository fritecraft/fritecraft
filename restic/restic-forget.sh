#!/bin/bash

source "$(dirname -- "$0")/config.sh"

restic forget --keep-last 24 --keep-daily 7 --keep-weekly 4 --keep-monthly 6 --keep-yearly 3
restic prune
