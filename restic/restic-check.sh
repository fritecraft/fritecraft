#!/bin/bash

source "$(dirname -- "$0")/config.sh"

restic version
restic snapshots
restic stats
restic check --with-cache
