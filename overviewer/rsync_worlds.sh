#!/bin/bash

rsync -rhvuPat /mnt/data/bouquenia/Bouquenia/ root@fritecraft.fr:/root/worlds/Bouquenia
rsync -rhvuPat /mnt/data/pitman/Pitman/ root@fritecraft.fr:/root/worlds/Pitman
rsync -rhvuPat /mnt/data/ressources/world/ root@fritecraft.fr:/root/worlds/world
rsync -rhvuPat /mnt/data/ressources/world_nether/ root@fritecraft.fr:/root/worlds/world_nether
rsync -rhvuPat /mnt/data/ressources/world_the_end/ root@fritecraft.fr:/root/worlds/world_the_end
rsync -rhvuPat /mnt/data/potos/world/ root@fritecraft.fr:/root/worlds/vanilla
