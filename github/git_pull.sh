#!/bin/bash

# Pull les fichiers pour Minecraft
# cd /home/minecraft/fritecraft && git pull

# Pull les fichiers pour le portfolio
echo "$(date +%y/%m/%d-%Hh%M)" >> /var/log/cron/github_portfolio.log
cd /var/www_cv && git pull https://github.com/Ventouz/Portfolio >> /var/log/cron/github_portfolio.log 2>&1

# Pull les fichiers pour le site de ecommerce
# cd /var/www_commerce && git pull

# Pull les scripts
echo "$(date +%y/%m/%d-%Hh%M)" >> /var/log/cron/github_scripts.log
cd /root/Scripts/ && git pull https://github.com/Ventouz/Scripts >> /var/log/cron/github_scripts.log 2>&1
