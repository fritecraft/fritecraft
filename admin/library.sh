#!/bin/bash

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/plugins_library.log 2>&1

backup_path="/mnt/minecraft/backups/plugins"
date=$(date +"%y-%m-%d")


cd /mnt/minecraft/
tar -cvf $backup_path/plugins-library-"$date.tar" library/

##############################################################################################################
# Recherche les fichiers correspondant au plugin et trie par date du plus ancien au plus recent
# puis filtre pour cacher le plus recent
# puis supprime les plus anciennes versions

cd /mnt/minecraft/library

ls -tr1 Actions*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 AdvancedFood*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 AnnouncerPlus*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 BottledExp*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 BetterRTP*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 BungeeTabListPlus_BukkitBridge*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 BungeeTabListPlus-*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 BungeeResourcepacks*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Citizens*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 ChestCommands*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 chunkmaster*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 Chunky*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 CMI-*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 CMIB*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 CMILib*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 CoreProtect*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Crazy-Auctions*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Crazy-Crates*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 CustomEnderChest*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 DAC*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 DeluxeMenus*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 DeluxeTags*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 DiscordSRV*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Dynmap*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 DynamicShop*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 dungeonsxl*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 EconomyShopGUI*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 EcoEnchants*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 FastAsyncWorldEdit*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 FogusCore*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Insights*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 InventorySaver*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 GadgetsMenu*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Gunging*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 GriefPrevention*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 gringotts*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 HeadDatabase*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 HuskHomes*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Jobs*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 LeakParkour*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 LibsDisguises*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 LuckPerms-Bukkit*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 LuckPerms-Bungee-*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 mcMMO*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MMOCore*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MMOItems*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MobHunting*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MysqlEconomyBank*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MysqlPlayerDataBridge*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MythicMobs*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 MythicLib*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 NexEngine.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 PhatLoots*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 PlaceholderAPI*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Plan*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 ProtocolLib*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Protocolize-bungeecord*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Protocolize-velocity*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 QuickShop*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 RestrictedCreative*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 spark*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 ServerSelectorX*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 Shopkeepers*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 TNE*.jar | head -n -1 | xargs --no-run-if-empty rm # Ajouter phase II
ls -tr1 Vault*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 VentureChat*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 WanderingTrades*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 worldedit-bukkit*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 worldguard-bukkit*.jar | head -n -1 | xargs --no-run-if-empty rm
ls -tr1 zstats*.jar | head -n -1 | xargs --no-run-if-empty rm

##############################################################################################################
