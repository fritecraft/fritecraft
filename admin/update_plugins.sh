#!/bin/bash

exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/var/log/plugins_update.log 2>&1

date=$(date +"%y-%m-%d")



##############################################################################################################


# Supprime le plugin dans "server" et copie la nouvelle version depuis la librairie vers "server"
#rm -v server/plugins/plugin ; cp -v /mnt/minecraft/library/plugin server/plugins/

plugin_updater () {
# BetterRTP
rm -v ressources/plugins/BetterRTP*.jar ; cp -v /mnt/minecraft/library/BetterRTP*.jar ressources/plugins/
rm -v survie/plugins/BetterRTP*.jar ; cp -v /mnt/minecraft/library/BetterRTP*.jar survie/plugins/
rm -v plots/plugins/BetterRTP*.jar ; cp -v /mnt/minecraft/library/BetterRTP*.jar plots/plugins/

# BottledExp
#rm -v server/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar server/plugins/
## hub
#rm -v hub/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar survie/plugins/
## ressources
rm -v ressources/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar proxy/plugins/
## plots
rm -v plots/plugins/BottledExp*.jar ; cp -v /mnt/minecraft/library/BottledExp*.jar plots/plugins/

# BungeeTabListPlus_BukkitBridge
#rm -v server/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar server/plugins/
## hub
rm -v hub/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar survie/plugins/
## ressources
rm -v ressources/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar proxy/plugins/
## plots
rm -v plots/plugins/BungeeTabListPlus_BukkitBridge*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus_BukkitBridge*.jar plots/plugins/

# BungeeTabListPlus
rm -v proxy/plugins/BungeeTabListPlus-*.jar ; cp -v /mnt/minecraft/library/BungeeTabListPlus-*.jar proxy/plugins/
rm -v proxy/plugins/BungeeResourcepacks*.jar ; cp -v /mnt/minecraft/library/BungeeResourcepacks*.jar proxy/plugins/

# Citizens
#rm -v server/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar server/plugins/
## hub
rm -v hub/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/Citizens*.jar ; cp -v /mnt/minecraft/library/Citizens*.jar proxy/plugins/

# CMI
rm -v hub/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar hub/plugins/
rm -v bouquenia/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar bouquenia/plugins/
rm -v survie/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar survie/plugins/
rm -v ressources/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar ressources/plugins/
rm -v aventure/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar aventure/plugins/
rm -v plots/plugins/CMI9-.jar ; cp -v /mnt/minecraft/library/CMI9-.jar plots/plugins/

# CMILib
rm -v hub/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar hub/plugins/
rm -v bouquenia/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar bouquenia/plugins/
rm -v survie/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar survie/plugins/
rm -v ressources/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar ressources/plugins/
rm -v aventure/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar aventure/plugins/
rm -v plots/plugins/CMILib*.jar ; cp -v /mnt/minecraft/library/CMILib*.jar plots/plugins/

# CoreProtect
rm -v hub/plugins/CoreProtect*.jar ; cp -v /mnt/minecraft/library/CoreProtect*.jar hub/plugins/
rm -v bouquenia/plugins/CoreProtect*.jar ; cp -v /mnt/minecraft/library/CoreProtect*.jar bouquenia/plugins/
rm -v survie/plugins/CoreProtect*.jar ; cp -v /mnt/minecraft/library/CoreProtect*.jar survie/plugins/

# CrazyCrates
rm -v bouquenia/plugins/CrazyCrates*.jar ; cp -v /mnt/minecraft/library/CrazyCrates*.jar bouquenia/plugins/

# CustomEnderChest
#rm -v server/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar server/plugins/
## hub
rm -v hub/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar survie/plugins/
## ressources
rm -v ressources/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar proxy/plugins/
## plots
rm -v plots/plugins/CustomEnderChest*.jar ; cp -v /mnt/minecraft/library/CustomEnderChest*.jar plots/plugins/

# DeluxeTags
#rm -v server/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar server/plugins/
## hub
rm -v hub/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar survie/plugins/
## ressources
rm -v ressources/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar proxy/plugins/
## plots
rm -v plots/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar plots/plugins/

# DeluxeMenus
#rm -v server/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar server/plugins/
## hub
rm -v hub/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar survie/plugins/
## ressources
rm -v ressources/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/DeluxeTags*.jar ; cp -v /mnt/minecraft/library/DeluxeTags*.jar proxy/plugins/
## plots
rm -v plots/plugins/DeluxeMenus*.jar ; cp -v /mnt/minecraft/library/DeluxeMenus*.jar plots/plugins/

# DiscordSRV
#rm -v server/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar server/plugins/
## hub
#rm -v hub/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/DiscordSRV*.jar ; cp -v /mnt/minecraft/library/DiscordSRV*.jar proxy/plugins/

# dungeonsXL
rm -v aventure/plugins/dungeonsxl*.jar ; cp -v /mnt/minecraft/library/dungeonsxl*.jar aventure/plugins/

# Dynmap
rm -v bouquenia/plugins/Dynmap*.jar ; cp -v /mnt/minecraft/library/Dynmap*.jar bouquenia/plugins/
rm -v ressources/plugins/Dynmap*.jar ; cp -v /mnt/minecraft/library/Dynmap*.jar ressources/plugins/
rm -v survie/plugins/Dynmap*.jar ; cp -v /mnt/minecraft/library/Dynmap*.jar survie/plugins/
rm -v vanilla/plugins/Dynmap*.jar ; cp -v /mnt/minecraft/library/Dynmap*.jar vanilla/plugins/
rm -v plots/plugins/Dynmap*.jar ; cp -v /mnt/minecraft/library/Dynmap*.jar plots/plugins/

# FastAsyncWorldEdit*.jar
rm -v hub/plugins/FastAsyncWorldEdit*.jar ; cp -v /mnt/minecraft/library/FastAsyncWorldEdit*.jar hub/plugins/
rm -v bouquenia/plugins/FastAsyncWorldEdit*.jar ; cp -v /mnt/minecraft/library/FastAsyncWorldEdit*.jar bouquenia/plugins/
rm -v aventure/plugins/FastAsyncWorldEdit*.jar ; cp -v /mnt/minecraft/library/FastAsyncWorldEdit*.jar aventure/plugins/
rm -v plots/plugins/FastAsyncWorldEdit*.jar ; cp -v /mnt/minecraft/library/FastAsyncWorldEdit*.jar plots/plugins/

# GadgetsMenu
#rm -v server/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar server/plugins/
## hub
rm -v hub/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar proxy/plugins/
## plots
rm -v plots/plugins/GadgetsMenu*.jar ; cp -v /mnt/minecraft/library/GadgetsMenu*.jar plots/plugins/

# GriefPrevention
#rm -v server/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar server/plugins/
## hub
rm -v hub/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/GriefPrevention*.jar ; cp -v /mnt/minecraft/library/GriefPrevention*.jar proxy/plugins/

# Gringotts
#rm -v server/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar server/plugins/
## hub
#rm -v hub/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/gringotts*.jar ; cp -v /mnt/minecraft/library/gringotts*.jar proxy/plugins/

# HeadDatabase
#rm -v server/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar server/plugins/
## hub
rm -v hub/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar proxy/plugins/
## aventure
rm -v plots/plugins/HeadDatabase*.jar ; cp -v /mnt/minecraft/library/HeadDatabase*.jar plots/plugins/

# InventorySaver
rm -v hub/plugins/InventorySaver*.jar ; cp -v /mnt/minecraft/library/InventorySaver*.jar hub/plugins/
rm -v bouquenia/plugins/InventorySaver*.jar ; cp -v /mnt/minecraft/library/InventorySaver*.jar bouquenia/plugins/
rm -v survie/plugins/InventorySaver*.jar ; cp -v /mnt/minecraft/library/InventorySaver*.jar survie/plugins/
rm -v ressources/plugins/InventorySaver*.jar ; cp -v /mnt/minecraft/library/InventorySaver*.jar hressourcesub/plugins/
rm -v plots/plugins/InventorySaver*.jar ; cp -v /mnt/minecraft/library/InventorySaver*.jar plots/plugins/


# Jobs
#rm -v server/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar server/plugins/
## hub
rm -v hub/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar survie/plugins/
## ressources
rm -v ressources/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar proxy/plugins/
## ressources
rm -v plots/plugins/Jobs*.jar ; cp -v /mnt/minecraft/library/Jobs*.jar plots/plugins/

# LibsDisguises
#rm -v server/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar server/plugins/
## hub
rm -v hub/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar survie/plugins/
## ressources
rm -v ressources/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar proxy/plugins/
## aventure
rm -v plots/plugins/LibsDisguises*.jar ; cp -v /mnt/minecraft/library/LibsDisguises*.jar plots/plugins/

# LuckPerms-Bukkit
#rm -v server/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar server/plugins/
## hub
rm -v hub/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar survie/plugins/
## ressources
rm -v ressources/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar proxy/plugins/
## aventure
rm -v plots/plugins/LuckPerms-Bukkit*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bukkit*.jar plots/plugins/

# LuckPerms-Bungee
rm -v proxy/plugins/LuckPerms-Bungee-*.jar ; cp -v /mnt/minecraft/library/LuckPerms-Bungee-*.jar proxy/plugins/

# MCMMO
#rm -v server/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar server/plugins/
## hub
rm -v hub/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar survie/plugins/
## ressources
rm -v ressources/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar proxy/plugins/
## aventure
rm -v plots/plugins/mcMMO*.jar ; cp -v /mnt/minecraft/library/mcMMO*.jar plots/plugins/

# MMOLib
#rm -v hub/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar hub/plugins/
#rm -v bouquenia/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar bouquenia/plugins/
#rm -v survie/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar survie/plugins/
#rm -v ressources/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar ressources/plugins/
#rm -v aventure/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar aventure/plugins/
#rm -v plots/plugins/MMOLib*.jar ; cp -v /mnt/minecraft/library/MMOLib*.jar plots/plugins/

# MMOCore
rm -v hub/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar hub/plugins/
rm -v bouquenia/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar bouquenia/plugins/
rm -v survie/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar survie/plugins/
rm -v ressources/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar ressources/plugins/
rm -v aventure/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar aventure/plugins/
rm -v plots/plugins/MMOCore*.jar ; cp -v /mnt/minecraft/library/MMOCore*.jar plots/plugins/

# MMOItems
rm -v hub/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar hub/plugins/
rm -v bouquenia/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar bouquenia/plugins/
rm -v survie/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar survie/plugins/
rm -v ressources/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar ressources/plugins/
rm -v aventure/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar aventure/plugins/
rm -v plots/plugins/MMOItems*.jar ; cp -v /mnt/minecraft/library/MMOItems*.jar plots/plugins/

# MythicLib
rm -v hub/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar hub/plugins/
rm -v bouquenia/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar bouquenia/plugins/
rm -v survie/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar survie/plugins/
rm -v ressources/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar ressources/plugins/
rm -v aventure/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar aventure/plugins/
rm -v plots/plugins/MythicLib*.jar ; cp -v /mnt/minecraft/library/MythicLib*.jar plots/plugins/

# MysqlEconomyBank
#rm -v server/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar server/plugins/
## hub
#rm -v hub/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar proxy/plugins/
## aventure
#rm -v plots/plugins/MysqlEconomyBank*.jar ; cp -v /mnt/minecraft/library/MysqlEconomyBank*.jar plots/plugins/

# MysqlPlayerDataBridge
#rm -v server/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar server/plugins/
## hub
##rm -v hub/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar proxy/plugins/
## aventure
#rm -v plots/plugins/MysqlPlayerDataBridge*.jar ; cp -v /mnt/minecraft/library/MysqlPlayerDataBridge*.jar plots/plugins/

# MythicMobs
#rm -v server/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar server/plugins/
## hub
#rm -v hub/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar survie/plugins/
## ressources
rm -v ressources/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar proxy/plugins/
## plots
rm -v plots/plugins/MythicMobs*.jar ; cp -v /mnt/minecraft/library/MythicMobs*.jar plots/plugins/

# PhatLoots
#rm -v server/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar server/plugins/
## hub
rm -v hub/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar survie/plugins/
## ressources
rm -v ressources/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar proxy/plugins/
## plots
rm -v plots/plugins/PhatLoots*.jar ; cp -v /mnt/minecraft/library/PhatLoots*.jar plots/plugins/

# PlaceholderAPI
#rm -v server/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar server/plugins/
## hub
#rm -v hub/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar survie/plugins/
## ressources
rm -v ressources/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar proxy/plugins/
## aventure
#rm -v plots/plugins/PlaceholderAPI*.jar ; cp -v /mnt/minecraft/library/PlaceholderAPI*.jar plots/plugins/

# Plan
#rm -v server/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar server/plugins/
## hub
rm -v hub/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar survie/plugins/
## ressources
rm -v ressources/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar aventure/plugins/
## proxy
rm -v proxy/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar proxy/plugins/
## plots
rm -v plots/plugins/Plan*.jar ; cp -v /mnt/minecraft/library/Plan*.jar plots/plugins/

# Playerpoints
#rm -v server/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar server/plugins/
## hub
rm -v hub/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar survie/plugins/
## ressources
rm -v ressources/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar aventure/plugins/
## proxy
rm -v proxy/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar proxy/plugins/
## plots
rm -v plots/plugins/PlayerPoints*.jar ; cp -v /mnt/minecraft/library/PlayerPoints*.jar plots/plugins/

# PlotSquared
rm -v plots/plugins/PlotSquared-Bukkit*.jar ; cp -v /mnt/minecraft/library/PlotSquared-Bukkit*.jar plots/plugins/


# ProtocolLib
#rm -v server/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar server/plugins/
## hub
rm -v hub/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar survie/plugins/
## ressources
rm -v ressources/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar proxy/plugins/
## aventure
rm -v plots/plugins/ProtocolLib*.jar ; cp -v /mnt/minecraft/library/ProtocolLib*.jar plots/plugins/

# RestrictedCreative
#rm -v server/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar server/plugins/
## hub
#rm -v hub/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/RestrictedCreative*.jar ; cp -v /mnt/minecraft/library/RestrictedCreative*.jar proxy/plugins/

# ServerSelectorX
#rm -v server/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar server/plugins/
## hub
rm -v hub/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar survie/plugins/
## ressources
rm -v ressources/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar proxy/plugins/
rm -v test/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar test/plugins/
## plots
rm -v plots/plugins/ServerSelectorX*.jar ; cp -v /mnt/minecraft/library/ServerSelectorX*.jar plots/plugins/


# Shopkeepers
#rm -v server/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar server/plugins/
## hub
rm -v hub/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/Shopkeepers*.jar ; cp -v /mnt/minecraft/library/Shopkeepers*.jar proxy/plugins/

# Spark
rm -v hub/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar hub/plugins/
rm -v bouquenia/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar bouquenia/plugins/
rm -v survie/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar survie/plugins/
rm -v ressources/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar ressources/plugins/
rm -v aventure/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar aventure/plugins/
rm -v plots/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar plots/plugins/
rm -v vanilla/plugins/spark*.jar ; cp -v /mnt/minecraft/library/spark*.jar vanilla/plugins/

# Vault
#rm -v server/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar server/plugins/
## hub
rm -v hub/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar survie/plugins/
## ressources
rm -v ressources/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar proxy/plugins/
rm -v test/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar test/plugins/
## plots
rm -v plots/plugins/Vault*.jar ; cp -v /mnt/minecraft/library/Vault*.jar plots/plugins/


# VentureChat
#rm -v server/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar server/plugins/
## hub
rm -v hub/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar bouquenia/plugins/
## survie
rm -v survie/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar survie/plugins/
## ressources
rm -v ressources/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar aventure/plugins/
## proxy
rm -v proxy/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar proxy/plugins/
## plots
rm -v plots/plugins/VentureChat*.jar ; cp -v /mnt/minecraft/library/VentureChat*.jar plots/plugins/

# worldedit
#rm -v server/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar server/plugins/
## hub
#rm -v hub/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar hub/plugins/
## bouquenia
#rm -v bouquenia/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar minigames/plugins/
## aventure
#rm -v aventure/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/worldedit-bukkit*.jar ; cp -v /mnt/minecraft/library/worldedit-bukkit*.jar proxy/plugins/

# worldguard
#rm -v server/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar server/plugins/
## hub
rm -v hub/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar hub/plugins/
## bouquenia
rm -v bouquenia/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar bouquenia/plugins/
## survie
#rm -v survie/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar survie/plugins/
## ressources
#rm -v ressources/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar ressources/plugins/
## vanilla
#rm -v vanilla/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar vanilla/plugins/
## minigames
#rm -v minigames/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar minigames/plugins/
## aventure
rm -v aventure/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar aventure/plugins/
## proxy
#rm -v proxy/plugins/worldguard-bukkit*.jar ; cp -v /mnt/minecraft/library/worldguard-bukkit*.jar proxy/plugins/  
  
}

cd /home/minecraft/servers
plugin_updater

cd /mnt/ramdisk/servers
plugin_updater

##############################################################################################################
echo "+++++++++++++++++++++++++++++"
