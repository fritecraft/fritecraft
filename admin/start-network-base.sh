#!/bin/bash

docker run --name=proxy -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=proxy \
-e MEMORY=512m -e INIT_MEMORY=512m -e MAX_MEMORY=512m \
-e TYPE=WATERFALL -e BUNGEE_JOB_ID=lastStableBuild -e WATERFALL_VERSION=latest -e WATERFALL_BUILD_ID=latest \
-v /mnt/ramdisk/servers/proxy:/server \
--network fritecraft \
-p 25565:25577 -p 80:8804 -p 9225:9225 \
itzg/bungeecord:latest

docker run --name=bouquenia -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=bouquenia \
-e ONLINE_MODE=FALSE -e EULA=TRUE -e USE_AIKAR_FLAGS=true \
-e MEMORY=6G -e INIT_MEMORY=6G -e MAX_MEMORY=6G \
-e TYPE=PAPER -e VERSION=1.19.2 -e TZ=Europe/Paris \
-v /mnt/ramdisk/servers/bouquenia:/data \
-v /mnt/ramdisk/volumes/DATAPACK/advancements:/data/Bouquenia/advancements/ \
-v /mnt/ramdisk/volumes/DATAPACK:/data/Bouquenia/datapacks/ \
-v /mnt/ramdisk/volumes/CMI:/data/plugins/CMI/ \
-v /mnt/ramdisk/volumes/ChestCommands:/data/plugins/ChestCommands/ \
-v /mnt/ramdisk/volumes/EconomyShopGUI:/data/plugins/EconomyShopGUI/ \
-v /mnt/ramdisk/volumes/DeluxeTags:/data/plugins/DeluxeTags/ \
-v /mnt/ramdisk/volumes/DeluxeMenus:/data/plugins/DeluxeMenus/ \
-v /mnt/ramdisk/volumes/GadgetsMenu:/data/plugins/GadgetsMenu/ \
-v /mnt/ramdisk/volumes/Jobs:/data/plugins/Jobs/ \
-v /mnt/ramdisk/volumes/mcMMO:/data/plugins/mcMMO/ \
-v /mnt/ramdisk/volumes/MythicLib:/data/plugins/MythicLib/ \
-v /mnt/ramdisk/volumes/MMOItems:/data/plugins/MMOItems/ \
-v /mnt/ramdisk/volumes/Oraxen:/data/plugins/Oraxen/ \
-v /mnt/ramdisk/volumes/PhatLoots:/data/plugins/PhatLoots/ \
-v /mnt/ramdisk/volumes/QuestWorld:/data/plugins/QuestWorld/ \
-v /mnt/ramdisk/volumes/RankVouchers:/data/plugins/RankVouchers/ \
-v /mnt/ramdisk/volumes/ServerSelectorX:/data/plugins/ServerSelectorX/ \
--network fritecraft \
-p 25581:25565 -p 31000:8123 -p 31225:9225 \
itzg/minecraft-server:latest

docker run --name=survie -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=survie \
-e ONLINE_MODE=FALSE -e EULA=TRUE -e USE_AIKAR_FLAGS=true \
-e MEMORY=6G -e INIT_MEMORY=6G -e MAX_MEMORY=6G \
-e TYPE=PAPER -e VERSION=1.19.2 -e TZ=Europe/Paris \
-v /mnt/ramdisk/servers/survie:/data \
-v /mnt/ramdisk/volumes/DATAPACK/advancements:/data/Pitman/advancements/ \
-v /mnt/ramdisk/volumes/DATAPACK:/data/Pitman/datapacks/ \
-v /mnt/ramdisk/volumes/CMI:/data/plugins/CMI/ \
-v /mnt/ramdisk/volumes/ChestCommands:/data/plugins/ChestCommands/ \
-v /mnt/ramdisk/volumes/EconomyShopGUI:/data/plugins/EconomyShopGUI/ \
-v /mnt/ramdisk/volumes/DeluxeTags:/data/plugins/DeluxeTags/ \
-v /mnt/ramdisk/volumes/DeluxeMenus:/data/plugins/DeluxeMenus/ \
-v /mnt/ramdisk/volumes/GadgetsMenu:/data/plugins/GadgetsMenu/ \
-v /mnt/ramdisk/volumes/Jobs:/data/plugins/Jobs/ \
-v /mnt/ramdisk/volumes/mcMMO:/data/plugins/mcMMO/ \
-v /mnt/ramdisk/volumes/MythicLib:/data/plugins/MythicLib/ \
-v /mnt/ramdisk/volumes/MMOItems:/data/plugins/MMOItems/ \
-v /mnt/ramdisk/volumes/PhatLoots:/data/plugins/PhatLoots/ \
-v /mnt/ramdisk/volumes/QuestWorld:/data/plugins/QuestWorld/ \
-v /mnt/ramdisk/volumes/RankVouchers:/data/plugins/RankVouchers/ \
-v /mnt/ramdisk/volumes/ServerSelectorX:/data/plugins/ServerSelectorX/ \
--network fritecraft \
-p 25582:25565 -p 32000:8123 -p 32225:9225 \
itzg/minecraft-server:latest

docker run --name=ressources -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=ressources \
-e ONLINE_MODE=FALSE -e EULA=TRUE -e USE_AIKAR_FLAGS=true \
-e MEMORY=6G -e INIT_MEMORY=6G -e MAX_MEMORY=6G \
-e TYPE=PAPER -e VERSION=1.19.2 -e TZ=Europe/Paris \
-v /mnt/ramdisk/servers/ressources:/data \
-v /mnt/ramdisk/volumes/DATAPACK/advancements:/data/world/advancements/ \
-v /mnt/ramdisk/volumes/CMI:/data/plugins/CMI/ \
-v /mnt/ramdisk/volumes/ChestCommands:/data/plugins/ChestCommands/ \
-v /mnt/ramdisk/volumes/EconomyShopGUI:/data/plugins/EconomyShopGUI/ \
-v /mnt/ramdisk/volumes/DeluxeTags:/data/plugins/DeluxeTags/ \
-v /mnt/ramdisk/volumes/DeluxeMenus:/data/plugins/DeluxeMenus/ \
-v /mnt/ramdisk/volumes/GadgetsMenu:/data/plugins/GadgetsMenu/ \
-v /mnt/ramdisk/volumes/Jobs:/data/plugins/Jobs/ \
-v /mnt/ramdisk/volumes/mcMMO:/data/plugins/mcMMO/ \
-v /mnt/ramdisk/volumes/MythicLib:/data/plugins/MythicLib/ \
-v /mnt/ramdisk/volumes/MMOItems:/data/plugins/MMOItems/ \
-v /mnt/ramdisk/volumes/PhatLoots:/data/plugins/PhatLoots/ \
-v /mnt/ramdisk/volumes/QuestWorld:/data/plugins/QuestWorld/ \
-v /mnt/ramdisk/volumes/RankVouchers:/data/plugins/RankVouchers/ \
-v /mnt/ramdisk/volumes/ServerSelectorX:/data/plugins/ServerSelectorX/ \
--network fritecraft \
-p 25590:25565 -p 39000:8123 -p 39225:9225 \
itzg/minecraft-server:latest

docker run --name=plots -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=plots \
-e ONLINE_MODE=FALSE -e EULA=TRUE -e USE_AIKAR_FLAGS=true \
-e MEMORY=10G -e INIT_MEMORY=10G -e MAX_MEMORY=10G \
-e TYPE=PAPER -e VERSION=1.19.2 -e TZ=Europe/Paris \
-v /mnt/ramdisk/servers/plots:/data \
-v /mnt/ramdisk/volumes/DATAPACK/advancements:/data/plots/advancements/ \
-v /mnt/ramdisk/volumes/CMI:/data/plugins/CMI/ \
-v /mnt/ramdisk/volumes/ChestCommands:/data/plugins/ChestCommands/ \
-v /mnt/ramdisk/volumes/EconomyShopGUI:/data/plugins/EconomyShopGUI/ \
-v /mnt/ramdisk/volumes/DeluxeTags:/data/plugins/DeluxeTags/ \
-v /mnt/ramdisk/volumes/DeluxeMenus:/data/plugins/DeluxeMenus/ \
-v /mnt/ramdisk/volumes/GadgetsMenu:/data/plugins/GadgetsMenu/ \
-v /mnt/ramdisk/volumes/Jobs:/data/plugins/Jobs/ \
-v /mnt/ramdisk/volumes/mcMMO:/data/plugins/mcMMO/ \
-v /mnt/ramdisk/volumes/MythicLib:/data/plugins/MythicLib/ \
-v /mnt/ramdisk/volumes/MMOItems:/data/plugins/MMOItems/ \
-v /mnt/ramdisk/volumes/PhatLoots:/data/plugins/PhatLoots/ \
-v /mnt/ramdisk/volumes/QuestWorld:/data/plugins/QuestWorld/ \
-v /mnt/ramdisk/volumes/RankVouchers:/data/plugins/RankVouchers/ \
-v /mnt/ramdisk/volumes/ServerSelectorX:/data/plugins/ServerSelectorX/ \
--network fritecraft \
-p 25585:25565 -p 35000:8123 -p 35225:9225 \
itzg/minecraft-server:latest
