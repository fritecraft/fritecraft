#!/bin/bash
docker run --name=hub -d \
--label io.podman.compose.project=scripts \
--label com.docker.compose.project=scripts \
--label com.docker.compose.service=hub \
-e ONLINE_MODE=FALSE -e EULA=TRUE -e USE_AIKAR_FLAGS=true \
-e MEMORY=2G -e INIT_MEMORY=2G -e MAX_MEMORY=2G \
-e TYPE=PAPER -e VERSION=1.18.2 -e TZ=Europe/Paris \
-v /mnt/ramdisk/servers/hub:/data \
-v /mnt/ramdisk/volumes/DATAPACK/advancements:/data/HUB/advancements/ \
-v /mnt/ramdisk/volumes/DATAPACK:/data/HUB/datapacks/ \
-v /mnt/ramdisk/volumes/CMI:/data/plugins/CMI/ \
-v /mnt/ramdisk/volumes/ChestCommands:/data/plugins/ChestCommands/ \
-v /mnt/ramdisk/volumes/DeluxeTags:/data/plugins/DeluxeTags/ \
-v /mnt/ramdisk/volumes/DeluxeMenus:/data/plugins/DeluxeMenus/ \
-v /mnt/ramdisk/volumes/GadgetsMenu:/data/plugins/GadgetsMenu/ \
-v /mnt/ramdisk/volumes/Jobs:/data/plugins/Jobs/ \
-v /mnt/ramdisk/volumes/mcMMO:/data/plugins/mcMMO/ \
-v /mnt/ramdisk/volumes/MMOLib:/data/plugins/MMOLib/ \
-v /mnt/ramdisk/volumes/MMOItems:/data/plugins/MMOItems/ \
-v /mnt/ramdisk/volumes/PhatLoots:/data/plugins/PhatLoots/ \
-v /mnt/ramdisk/volumes/QuestWorld:/data/plugins/QuestWorld/ \
-v /mnt/ramdisk/volumes/RankVouchers:/data/plugins/RankVouchers/ \
-v /mnt/ramdisk/volumes/ServerSelectorX:/data/plugins/ServerSelectorX/ \
--network fritecraft \
-p 25580:25565 -p 30225:9225 \
itzg/minecraft-server:latest